import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActivityListComponent } from './activity-list/activity-list.component';
import { EntriesResolver as EntriesResolver } from './activity-list/entries.resolver';
import { TokenResolver } from './resolvers/token.resolver';
import { DateResolver } from './new-entry/date.resolver';


const routes: Routes = [
  {
    path: '',
    component: ActivityListComponent,
    resolve: { entries: EntriesResolver, token: TokenResolver },
    runGuardsAndResolvers: 'pathParamsOrQueryParamsChange'
  },
  {
    path: 'new-entry',
    loadChildren: () => import('./new-entry/new-entry.module').then(m => m.NewEntryModule),
    resolve: { token: TokenResolver, date: DateResolver }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
