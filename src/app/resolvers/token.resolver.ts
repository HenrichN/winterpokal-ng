import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { of as observableOf } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TokenResolver implements Resolve<any> {

  constructor() { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return observableOf(route.fragment ? route.fragment : '');
  }
}
