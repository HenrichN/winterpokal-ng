import { Moment } from 'moment';
import { Activity } from './activity.interface';

export interface Entry {
  date: Moment;
  activities: Activity[];
}
