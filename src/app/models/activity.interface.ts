export type Category =
  'laufen'
  | 'radfahren'
  | 'alternative_sportarten';

export interface Activity {
  entry: {
    id: string;
    category: string;
    date: string;
    description: string;
    duration: number;
    distance: number;
    nightride: boolean;
    points: number;
    created: string;
  };
  category: {
    id: Category;
    title: string;
  };
  user: {
    id: string;
    name: string;
    entries: string;
    points: string;
    duration: string;
    url: string;
  };
}

export interface NewActivity {
  category: string;
  date: string;
  description: string;
  duration: number;
  distance: number;
  nightride: boolean;
}
