import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { NewEntryRoutingModule } from './new-entry-routing.module';
import { LoadingScreenModule } from '../loading-screen/loading-screen.module';
import { NewEntryComponent } from './new-entry.component';


@NgModule({
  declarations: [NewEntryComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatSnackBarModule,
    MatToolbarModule,
    FontAwesomeModule,
    NewEntryRoutingModule,
    LoadingScreenModule
  ]
})
export class NewEntryModule { }
