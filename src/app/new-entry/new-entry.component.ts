import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UntypedFormBuilder } from '@angular/forms';
import { Category, NewActivity } from '../models/activity.interface';
import { MatSnackBar } from '@angular/material/snack-bar';
import { faArrowLeft, faCheck } from '@fortawesome/free-solid-svg-icons';
import { ActivitiesService } from '../services/activites.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-new-entry',
  templateUrl: './new-entry.component.html',
  styleUrls: ['./new-entry.component.css']
})
export class NewEntryComponent implements OnInit {
  loading = false;
  token = '';
  date = '';
  dateToDisplay = '';

  faArrowLeft = faArrowLeft;
  faCheck = faCheck;

  form = this.fb.group({
    category: ['radfahren'],
    duration: [40],
    distance: [13],
    description: ['MdRzAuz']
  });

  readonly activities: { value: Category, displayValue: string }[] = [
    { value: 'radfahren', displayValue: 'Radfahren' },
    { value: 'laufen', displayValue: 'Laufen' },
    { value: 'alternative_sportarten', displayValue: 'Alternativ' }
  ];

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private fb: UntypedFormBuilder,
    private snackBar: MatSnackBar,
    private activitiesService: ActivitiesService
  ) {
    this.activatedRoute.data.subscribe(data => {
      this.token = data.token;
      this.date = data.date;
      this.dateToDisplay = this.date?.split('-').reverse().join('-');
    });

    if (!this.token) {
      this.snackBar.open('Token missing', 'Error', {
        duration: 3000,
      });
    }
  }

  ngOnInit() {
  }

  cancel() {
    this.router.navigate(['/'], {
      fragment: this.token
    });
  }

  create() {
    this.loading = true;
    const newActivity: NewActivity = {
      ... this.form.value,
      ... { distance: this.form.value.distance * 1000 },
      ... { nightride: false },
      ... { date: this.date }
    };
    this.activitiesService.createActivity(this.token, newActivity)
      .pipe(finalize(() => this.loading = false))
      .subscribe({
        next: _ => this.cancel(),
        error: _ => {
          this.snackBar.open('Unable to create activity', 'Error', {
            duration: 3000,
          });
        }
      });
  }

}
