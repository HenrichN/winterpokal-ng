import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Entry } from '../models/entry.interface';
import { Activity } from '../models/activity.interface';
import { faBiking, faRunning, faDumbbell } from '@fortawesome/free-solid-svg-icons';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-activity-list',
  templateUrl: './activity-list.component.html',
  styleUrls: ['./activity-list.component.css']
})
export class ActivityListComponent implements OnInit {
  private token = '';
  showShowAllButton = true;
  entries: Entry[] = [];

  faBiking = faBiking;
  faRunning = faRunning;
  faDumbbell = faDumbbell;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private snackBar: MatSnackBar
  ) {
    this.activatedRoute.data.subscribe(data => {
      this.entries = data.entries.reverse();
      this.token = data.token;

      if (!this.token) {
        this.snackBar.open('Token missing', 'Error', {
          duration: 3000,
        });
      }
    });

    this.activatedRoute.queryParamMap.subscribe(queryParamsMap => {
      this.showShowAllButton = !(queryParamsMap.get('days') === 'all');
    });
  }

  ngOnInit() {
  }

  navigateToNewEntry(entry: Entry) {
    this.router.navigate(['new-entry'], {
      queryParams: { date: entry.date.format('YYYY-MM-DD') },
      fragment: this.token
    });
  }

  showAll() {
    const queryParams = { days: 'all' };
    this.router.navigate([], {
      relativeTo: this.activatedRoute,
      queryParams,
      queryParamsHandling: 'merge',
      fragment: this.token
    });
  }

  isCyclingActivity(activity: Activity) {
    return activity.category.id === 'radfahren';
  }

  isRunningActivity(activity: Activity) {
    return activity.category.id === 'laufen';
  }

  isOtherActivity(activity: Activity) {
    return activity.category.id === 'alternative_sportarten';
  }

}
