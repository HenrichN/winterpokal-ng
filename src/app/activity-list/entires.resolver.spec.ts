import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { EntriesResolver } from './entries.resolver';
import { ActivitiesService } from '../services/activites.service';
import { Activity } from '../models/activity.interface';

describe('EntriesResolver', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [ActivitiesService]
  }));

  it('should map the queryParam "entries" correctly', () => {
    const service: EntriesResolver = TestBed.inject(EntriesResolver);
    expect(service.mapQueryParameterDays(null)).toBe(21);
    expect(service.mapQueryParameterDays('something')).toBe(21);
    expect(service.mapQueryParameterDays('all')).toBe(-1);
    expect(service.mapQueryParameterDays('-1')).toBe(21);
    expect(service.mapQueryParameterDays('0')).toBe(0);
    expect(service.mapQueryParameterDays('20')).toBe(20);
  });

  it('should compute the date of the first activity correctly', () => {
    const service: EntriesResolver = TestBed.inject(EntriesResolver);

    let activites = [
      { entry: { date: '2020-10-18' } } as Activity,
      { entry: { date: '2020-10-19' } } as Activity,
      { entry: { date: '2020-10-20' } } as Activity
    ];
    expect(service.dateOfFirstActivity(activites)).toBe('2020-10-18');

    activites = [
      { entry: { date: '2020-10-20' } } as Activity,
      { entry: { date: '2020-10-19' } } as Activity,
      { entry: { date: '2020-10-18' } } as Activity
    ];
    expect(service.dateOfFirstActivity(activites)).toBe('2020-10-18');
  });
});
