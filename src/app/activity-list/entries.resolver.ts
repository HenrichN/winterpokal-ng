import { Injectable } from '@angular/core';
import { DaysService } from '../services/days.service';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { of as observableOf, from as observableFrom, Observable } from 'rxjs';
import { ActivitiesService, AllActivitiesResponse } from '../services/activites.service';
import { EntriesService } from '../services/entries.service';
import { Activity } from '../models/activity.interface';

@Injectable({
  providedIn: 'root'
})
export class EntriesResolver implements Resolve<any> {

  private defaultDays = 21;

  constructor(
    private daysService: DaysService,
    private activitiesService: ActivitiesService,
    private entriesService: EntriesService
  ) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const token = route.fragment;
    const defaultDate = moment().subtract(this.defaultDays, 'days').format('YYYY-MM-DD');
    if (token) {
      let activitiesObservable: Observable<AllActivitiesResponse>;
      const fastlane = (window as any).fastlane;
      if (fastlane) {
        activitiesObservable = observableFrom(fastlane.then((data: any) => data.json())) as Observable<AllActivitiesResponse>;
        (window as any).fastlane = null;
      } else {
        activitiesObservable = this.activitiesService.getAllActivities(token);
      }
      return activitiesObservable
        .pipe(
          map(activities => {
            const days = this.mapQueryParameterDays(route.queryParamMap.get('days'));
            const startDate = days < 0 ? this.dateOfFirstActivity(activities.data) : moment().subtract(days, 'days').format('YYYY-MM-DD');
            return this.entriesService.merge(this.daysService.days(startDate), activities.data);
          }));
    } else {
      return observableOf(this.entriesService.merge(this.daysService.days(defaultDate), []));
    }
  }

  mapQueryParameterDays(queryParam: null | string): number {
    if (queryParam === null) {
      return this.defaultDays;
    } else if (queryParam === 'all') {
      return -1;
    } else {
      const numberOfEntries = parseInt(queryParam, 10);
      if (isNaN(numberOfEntries)) {
        return this.defaultDays;
      } else {
        if (numberOfEntries < 0) {
          return this.defaultDays;
        } else {
          return numberOfEntries;
        }
      }
    }
  }

  dateOfFirstActivity(activities: Activity[]): string {
    const firstEntry = moment(activities[0].entry.date);
    const lastEntry = moment(activities[activities.length - 1].entry.date);

    if (firstEntry.isBefore(lastEntry)) {
      return firstEntry.format('YYYY-MM-DD');
    } else {
      return lastEntry.format('YYYY-MM-DD');
    }
  }
}
