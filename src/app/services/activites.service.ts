import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Activity, NewActivity } from '../models/activity.interface';
import { Observable } from 'rxjs/internal/Observable'; // CHECK ME!!
import { of as observableOf } from 'rxjs';
import * as moment from 'moment';

export interface Response<T> {
  status: string;
  messages: string[];
  data: T;
}

export type AllActivitiesResponse = Response<Activity[]>;
export type CreateActivityResponse = Response<Activity>;

@Injectable({
  providedIn: 'root'
})
export class ActivitiesService {
  constructor(private http: HttpClient) { }

  public getAllActivities(apiToken: string, dev: boolean = false): Observable<AllActivitiesResponse> {
    if (!dev) {
      const httpOptions = {
        headers: new HttpHeaders({
          'api-token': apiToken
        })
      };

      return this.http.get<AllActivitiesResponse>(
        'https://cors-anywhere.herokuapp.com/https://winterpokal.mtb-news.de/api/v1/entries/my.json',
        httpOptions);
    } else {
      return observableOf(this.mockResponse());
    }
  }

  public createActivity(apiToken: string, newActivity: NewActivity): Observable<CreateActivityResponse> {
    const httpOptions = {
      headers: new HttpHeaders({
        'api-token': apiToken
      })
    };

    return this.http.post<CreateActivityResponse>(
      'https://cors-anywhere.herokuapp.com/https://winterpokal.mtb-news.de/api/v1/entries/add.json',
      newActivity,
      httpOptions);
  }

  private mockResponse(): AllActivitiesResponse {
    return {
      status: 'OK',
      messages: [],
      data: [
        {
          entry: {
            id: '1',
            category: 'Laufen',
            date: moment().format('YYYY-MM-DD'),
            description: 'Some running',
            duration: 56,
            distance: 9100,
            nightride: false,
            points: 2,
            created: ''
          },
          category: {
            id: 'laufen',
            title: ''
          },
          user: {
            id: '123',
            name: 'MockUser',
            entries: '',
            points: '',
            duration: '',
            url: ''
          }
        },
        {
          entry: {
            id: '1',
            category: 'Radfahren',
            date: moment().format('YYYY-MM-DD'),
            description: 'Some cycling',
            duration: 56,
            distance: 9100,
            nightride: false,
            points: 2,
            created: ''
          },
          category: {
            id: 'radfahren',
            title: ''
          },
          user: {
            id: '123',
            name: 'MockUser',
            entries: '',
            points: '',
            duration: '',
            url: ''
          }
        },
        {
          entry: {
            id: '2',
            category: 'Alternative Sportarten',
            date: moment().subtract(1, 'days').format('YYYY-MM-DD'),
            description: 'Some other sport',
            duration: 150,
            distance: 0,
            nightride: false,
            points: 2,
            created: '2019-12-21 14:58:48'
          },
          category: {
            id: 'alternative_sportarten',
            title: ''
          },
          user: {
            id: '123',
            name: 'MockUser',
            entries: '',
            points: '',
            duration: '',
            url: ''
          }
        },
        {
          entry: {
            id: '3',
            category: 'Radfahren',
            date: moment().subtract(2, 'days').format('YYYY-MM-DD'),
            description: 'Some cycling',
            duration: 30,
            distance: 10000,
            nightride: false,
            points: 2,
            created: ''
          },
          category: {
            id: 'radfahren',
            title: ''
          },
          user: {
            id: '123',
            name: 'MockUser',
            entries: '',
            points: '',
            duration: '',
            url: ''
          }
        },
        {
          entry: {
            id: '3',
            category: 'Alternative',
            date: moment().subtract(2, 'days').format('YYYY-MM-DD'),
            description: 'Something else',
            duration: 30,
            distance: 10000,
            nightride: false,
            points: 2,
            created: ''
          },
          category: {
            id: 'alternative_sportarten',
            title: ''
          },
          user: {
            id: '123',
            name: 'MockUser',
            entries: '',
            points: '',
            duration: '',
            url: ''
          }
        }
      ]
    };
  }
}
