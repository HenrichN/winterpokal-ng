import { TestBed } from '@angular/core/testing';
import { EntriesService } from './entries.service';
import * as moment from 'moment';
import { Activity } from '../models/activity.interface';

describe('EntriesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should merge entries correctly with more days than activities', () => {
    const service: EntriesService = TestBed.inject(EntriesService);
    const days = [
      moment('2019-10-18'),
      moment('2019-10-19'),
      moment('2019-10-20'),
      moment('2019-10-21')
    ];
    const activites = [
      { entry: { date: '2019-10-18' } } as Activity,
      { entry: { date: '2019-10-20' } } as Activity,
      { entry: { date: '2019-10-20' } } as Activity
    ];

    const result = service.merge(days, activites);
    expect(result.length).toBe(4);
    expect(result[0].activities.length).toBe(1);
    expect(result[2].activities.length).toBe(2);
  });

  it('should merge entries correctly with more activities than days', () => {
    const service: EntriesService = TestBed.inject(EntriesService);
    const days = [
      moment('2019-10-18'),
      moment('2019-10-19'),
      moment('2019-10-20')
    ];
    const activites = [
      { entry: { date: '2019-10-17' } } as Activity,
      { entry: { date: '2019-10-18' } } as Activity,
      { entry: { date: '2019-10-19' } } as Activity,
      { entry: { date: '2019-10-20' } } as Activity,
      { entry: { date: '2019-10-21' } } as Activity
    ];

    const result = service.merge(days, activites);
    expect(result.length).toBe(3);
    expect(result[0].activities.length).toBe(1);
    expect(result[1].activities.length).toBe(1);
    expect(result[2].activities.length).toBe(1);
  });

});
