import { Injectable } from '@angular/core';
import { Activity } from '../models/activity.interface';
import * as moment from 'moment';
import { Moment } from 'moment';
import { Entry } from '../models/entry.interface';

@Injectable({
  providedIn: 'root'
})
export class EntriesService {

  constructor() { }

  merge(days: Moment[], activities: Activity[]): Entry[] {
    return days.map(day => ({
      date: day,
      activities: activities.filter(activity => moment(activity.entry.date).isSame(day))
    }));
  }
}
