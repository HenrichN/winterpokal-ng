import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Moment } from 'moment';
import 'moment/locale/de';

@Injectable({
  providedIn: 'root'
})
export class DaysService {
  /**
   *
   * @param since YYYY-MM-DD
   */
  public days(since: string): Moment[] {
    const start = moment(since);
    const end = moment();
    return this.dateRange(start, end);
  }

  private dateRange(start: Moment, end: Moment): Moment[] {
    const days = [];
    for (const m = moment(start); m.diff(end, 'days') <= 0; m.add(1, 'days')) {
      days.push(moment(m));
    }
    days.pop();
    return days;
  }
}
